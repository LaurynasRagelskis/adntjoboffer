<?php 
/*
 * Visos naudojamos klases
 * (panaudojimas labiau salyginis, negu realiai prasmingas):
 * 
 * @Product
 * @Category
 * @ProductsList
 * @CategoryList
 * 
 */

class Product {

   var $id;
   var $name;
   var $price;
   var $category;

   function Product($id, $name, $price, $category)    // uzpildoma nuskaitant DB suomenis
   {
       $this->id = $id;
       $this->name = $name;
       $this->price = $price;
       $this->category = $category;
   }

   function print_info()
   { 
       $pvm =  number_format($this->price * 0.21, 2);
       $out = "#" . $this->id . " " . $this->name . " " . $this->price . " (PVM " . $pvm . ")"; //isvedimo formatas
       return $out;
   }

   function set_price($newPrice) 
   {
       $this->price = $newPrice;
   }
   
} // end of class Product

class Category {

   var $id;
   var $name;
   var $productsCount;

   function Category($id, $name, $productsCount)       // uzpildoma nuskaitant DB suomenis
   {
       $this->id = $id;
       $this->name = $name;
       $this->productsCount = $productsCount;
   }

   function print_info()
   { 
       $out = "#" . $this->id . " " . $this->name . " (" . $this->productsCount . ")"; //isvedimo formatas
       return $out;
   }

   function add_product() 
   {
       $this->productsCount++;
   }
   
} // end of class Category
    

class ProductsList {
    
    var $id;
    var $products = array();
    var $amount = 0;
    static $total = 0;
    
    function ProductsList() 
    {
        $this->total++;
        $this->id = $this->total;
    }
    
    function add_new_product($new_product) 
    {
       $this->products[] = $new_product;
       $this->amount++;
    }
    
    function print_products_list()              // testavimui 
    {
        print_r($this->products);
    }     
} // end of class ProductsList

class CategoriesList {
    
    var $id;
    var $categories = array();
    var $amount = 0;
    static $total = 0;
    
    function CategoriesList() 
    {
        $this->total++;
        $this->id = $this->total;
    }
    
    function add_new_category($new_category) 
    {
       $this->categories[] = $new_category;
       $this->amount++;
    }
    
    function print_categories_list()             // testavimui
    {
        echo "List ID     : $this->id\n";
        echo "Cat. count  : $this->amount\n";
        echo "Total lists : $this->total\n\n";
        print_r($this->categories);
    } 
} // end of class CategoriesList

?>
