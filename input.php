 <?php
/*  
 *  Laurynas Ragelskis
 *  laurynas.ragelskis@gmail.com
 * 
 *  Versija su klasemis
 *
 */

require('Smarty.class.php');
$smarty = new Smarty;
$smarty->template_dir = './templates';
$smarty->config_dir   = './config';
$smarty->cache_dir    = '../smarty/cache';
$smarty->compile_dir  = '../smarty/templates_c';

$include_path = './include';
require_once($include_path . '/sql.php');
include_once $include_path . '/classes.inc';

$xml     = @file_get_contents('php://input');
$xmlData = new SimpleXMLElement($xml);
$action  = $xmlData->action;
$outputData = array();

switch ($action) {
    case "getCategories":
        $sqlData = get_data_from_DB($action, "", "");       

        $categoryList = new CategoriesList();

        for ($i=0; $i<sizeof($sqlData); $i++) {
            $category = new Category($sqlData[$i]["category_id"],
                                     $sqlData[$i]["category_name"],
                                     $sqlData[$i]["products"]);
            $categoryList->add_new_category($category);
            $outputData[] = $categoryList->categories[$i]->print_info();
        }
        break;
        
    case "getProducts":
        $catID   = $xmlData->params->catid;
        $page    = $xmlData->params->page;
        $sqlData = get_data_from_DB($action, $catID, $page);
        
        $productList = new ProductsList();
        
        for ($i=0; $i<sizeof($sqlData); $i++) {
            $product = new Product($sqlData[$i]["product_id"],
                                   $sqlData[$i]["product_name"],
                                   $sqlData[$i]["price"],
                                   $sqlData[$i]["category_id"]);
            $productList->add_new_product($product);
            $outputData[] = $productList->products[$i]->print_info();
        }
        break;
    
    default:
        exit();
        break;
}

$smarty->assign('data', $outputData);

$smarty->display( ($action == "getCategories") ? 'input_category.tpl' : 'input_product.tpl');
?>